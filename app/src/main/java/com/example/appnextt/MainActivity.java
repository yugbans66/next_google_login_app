package com.example.appnextt;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private String email;
    private final int animDist = 200;
    private float animStart;
    private int animDuration = 1000;
    private SignInButton signInButton;
    private TextView statusText;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        GoogleSignIn.getLastSignedInAccount(this);

        signInButton = findViewById(R.id.signInButton);
        statusText = findViewById(R.id.statusText);
        animStart = statusText.getTranslationY();

        new Handler().postDelayed(() -> {
            showView(signInButton);
            hideView(statusText);
            signInButton.setSize(SignInButton.SIZE_STANDARD);
            signInButton.setOnClickListener(v -> signIn());
        },1000);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                email = account.getEmail();
                hideView(signInButton);
                statusText.setTextColor(Color.WHITE);
                showStatus("Signed In, Getting location");
                turnGpsOn();
            }
        } catch (ApiException e) {
            Log.d("purplepurple", e.toString());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void turnGpsOn() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, locationSettingsResponse -> getLoc());

        task.addOnFailureListener(this, e -> {
            if (e instanceof ResolvableApiException) {
                ResolvableApiException resolvable = (ResolvableApiException) e;
                try {
                    resolvable.startResolutionForResult(MainActivity.this, 51);
                } catch (IntentSender.SendIntentException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getLoc() {
        if ((ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {

            fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
                if (location != null) {
                    addToSpredsheet(email, location);
                    hideView(statusText);
                    showStatus("Got the location! Uploading to spreadsheet");
                }
            });
        } else
            requestPermissions(new String[]{ACCESS_FINE_LOCATION}, 12);
    }

    private void addToSpredsheet(String email, Location location) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://script.google.com/macros/s/AKfycbweISUvLVPrj1YqvxcYIJ1u-Z0BUUcMJ2REmThuZA/exec",
                response -> {
                    if(response.equals("Success")){
                        showStatus("Done, Click me to view spreadsheet!");
                        hideView(statusText);
                        statusText.setOnClickListener(view -> {
                            AnimatorSet as = new AnimatorSet();
                            as.playSequentially(ObjectAnimator.ofArgb(statusText, "backgroundColor", Color.WHITE, Color.LTGRAY).setDuration(300)
                                    , ObjectAnimator.ofArgb(statusText, "backgroundColor", Color.LTGRAY, Color.WHITE).setDuration(300));
                            as.setInterpolator(new DecelerateInterpolator());
                            as.start();
                            String url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vR14JCuhuflqSNU6GA7RmHgmK1sYkCpyXwn-JVUe2c9AOLnS9UTgPjs7HK15PHBnmjN_i5trvjWITqi/pubhtml";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            startActivity(i);
                        });
                    }
                },
                error -> Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show()
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("action", "addItem");
                params.put("email", email);
                params.put("loc", location.getLatitude() + "'" + location.getLongitude() + "\"");

                return params;
            }
        };

        int socketTimeout = 10000;

        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 12) if (resultCode == RESULT_OK) getLoc();

        if (requestCode == 51) {
            if (resultCode == RESULT_CANCELED) turnGpsOn();
            else getLoc();
        }

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void showView(View v) {
        ObjectAnimator.ofFloat(v, "translationY", animStart + animDist, animStart).setDuration(animDuration).start();
        ObjectAnimator.ofFloat(v, "alpha", 0, 1).setDuration(animDuration).start();
    }

    private void showStatus(String status) {
        ObjectAnimator oa = ObjectAnimator.ofFloat(statusText, "translationY", animStart + animDist, animStart).setDuration(animDuration);
        ObjectAnimator oa2 = ObjectAnimator.ofFloat(statusText, "alpha", 0, 1).setDuration(animDuration);

        oa.setStartDelay(animDuration);
        oa2.setStartDelay(animDuration);

        oa.start();
        oa2.start();

        new Handler().postDelayed(() -> {
            statusText.setText(status);
            statusText.setTextColor(Color.BLACK);
        },animDuration);
    }

    private void hideView(View v) {
        ObjectAnimator.ofFloat(v, "translationY", animStart, animStart - animDist).setDuration(animDuration).start();
        ObjectAnimator.ofFloat(v, "alpha", 1, 0).setDuration(animDuration).start();
    }
}